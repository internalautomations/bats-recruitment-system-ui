import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public url: string = "";

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.url = this.router.url;
  }

  logout(){
    this.authService.logout();
  }

}

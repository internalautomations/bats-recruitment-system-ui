import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigService } from 'src/app/api.service';
import {
  FormControl,
  FormGroupDirective,
  FormBuilder,
  FormGroup,
  NgForm,
  Validators,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { AuthService } from 'src/app/auth.service';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  loginErr: boolean = false;
  logInForm: FormGroup;
  username: string;
  password: string;
  token: object;
  isLoading = false;

  constructor(
    private router: Router,
    private api: ConfigService,
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.logInForm = this.formBuilder.group({
      username: [null, Validators.required],
      password: [null, Validators.required],
    });
  }

  LoginUser() {
    this.isLoading = true;
    this.api
      .auth(this.logInForm.value.username, this.logInForm.value.password)
      .subscribe(
        (res: any) => {
          this.isLoading = false;
          if (res.body) {
            this.token = res.body[0];
            console.log(res.body);
            localStorage.setItem('ACCESS_TOKEN', res.body.token);
            localStorage.setItem('USER', res.body.username);
            if (this.authService.isLoggedIn())
              this.router.navigateByUrl('/portal');
            else this.router.navigateByUrl('/login');
          } else {
            //console.log(res.body);
            //console.log('Failed');
            this.isLoading = false;
            this.loginErr = true;
          }
        },
        (err) => {
          this.isLoading = false;
          this.loginErr = true;
          console.log(err);
        }
      );
  }

}

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ConfigService } from '../../api.service';
import { Candidate } from '../struct/candidate';
@Component({
  selector: 'app-candidate',
  templateUrl: './candidate.component.html',
  styleUrls: ['./candidate.component.css'],
})
export class CandidateComponent implements OnInit {
  @ViewChild('searchCandidateByStatus')
  searchCandidateByStatusField: ElementRef;
  @ViewChild('searchCandidateByRegNo') searchCandidateByRegNoField: ElementRef;
  @ViewChild('searchCandidateByRating') searchCandidateByRatingField: ElementRef;
  @ViewChild('toggleAlertModal')
  toggleAlertModal: any;
  @ViewChild('selectCandidateModal') selectCandidateModal: ElementRef;
  @ViewChild('rejectCandidateModal') rejectCandidateModal: ElementRef;
  @ViewChild('skipCandidateModal') skipCandidateModal: ElementRef;
  isLoading = true;
  isSearchable = true;
  errorMessage = '';
  searchCategory = 'status';
  statuses = [];
  candidates: Candidate[] = [];
  InterviewDetails = {
    Name: '',
    Current_Status: '',
    Next_Status: '',
    FeedBack: '',
    Rating: ''
  };
  MapShowValues = {
      ON_SUB: "ONLINE SUBMISSION",
      MANUAL_PRI : "MANUAL SCREENING",
      OQUIZ_WAIT: "ONLINE QUIZ",
      TELEPHONE_WAIT  : "TELEPHONIC SCREENING",
      BARE_ESS_WAIT   : "BARE ESSENTIALS",
      TECH1_WAIT      : "TECHNICAL 1",
      TECH2_WAIT      : "TECHNICAL 2",
      BOSS_WAIT       : "BOSS ROUND",
      HR_WAIT         : "HR",
      SELECTED :  "RELEASE OFFER",
      REJECTED :  "REJECTED",
      DUPLICATE : "DUPLICATE"
  };
  MapActualValues = {
    "ONLINE SUBMISSION":"ON_SUB",
    "MANUAL SCREENING":"MANUAL_PRI",
    "ONLINE QUIZ":"OQUIZ_WAIT",
    "TELEPHONIC SCREENING":"TELEPHONE_WAIT",
    "BARE ESSENTIALS":"BARE_ESS_WAIT",
    "TECHNICAL 1":"TECH1_WAIT",
    "TECHNICAL 2":"TECH2_WAIT",
    "BOSS ROUND":"BOSS_WAIT",
    "HR":"HR_WAIT",
    "RELEASE OFFER":"SELECTED",
    "REJECTED":"REJECTED",
    "DUPLICATE":"DUPLICATE"
  };
  avaliableStatus = [];
  user: string;
  isAdmin= false;

  constructor(private api: ConfigService) {}
  ngOnInit() {
    this.user= String(localStorage.getItem('USER'));
    if(this.user=='admin' ||  this.user=='vijayrao' || this.user=='venkut')
        this.isAdmin = true;
    this.api.getAllStatus().subscribe(
      (res: any) => {
        this.statuses = res.body;
        for(let i=0;i<this.statuses.length;i++){
          this.statuses[i]=this.MapShowValues[this.statuses[i]];
        }
        // console.log("Admin: ",this.isAdmin);
        
        if(!this.isAdmin){
          this.removeStatus('RELEASE OFFER');
          this.removeStatus('BOSS ROUND');
        }
        //console.log(this.statuses);
      },
      (err) => {
        // console.log(err);
        // this.errorMessage = 'Failed to get list of status !';
        // this.toggleAlertModal.nativeElement.click();
        alert('Failed to get list of status !');
      }
    );
    this.isLoading = false;
  }
  
  changeSearchCategory(category) {
    this.searchCategory = category;
  }
  getSearchText(event) {
    if (event.keyCode == 13) {
      this.getCandidate();
    }
  }
  getCandidate() {
    this.isLoading = true;
    this.isSearchable = false;
    var searchText;
    var queryParam;
    if (this.searchCategory == 'status') {
      let searchValue = this.searchCandidateByStatusField.nativeElement.value.trim();
      searchText = this.MapActualValues[searchValue];
      queryParam = {
        status: String(searchText),
        username: String(localStorage.getItem('USER')),
      };
    } else if(this.searchCategory == 'manual_rating'){
      searchText = this.searchCandidateByRatingField.nativeElement.value.trim();
      queryParam = {
        manual_rating: String(searchText),
        username: String(localStorage.getItem('USER')),
      };
    } else {
      searchText = this.searchCandidateByRegNoField.nativeElement.value.trim();
      queryParam = {
        reg_no: String(searchText),
        username: String(localStorage.getItem('USER')),
      };
    }
    
        

    if (searchText) {
      // console.log(queryParam);
      this.api.getCandidate(queryParam).subscribe(
        (res: any) => {
          if (res.body.length) {
            this.candidates = res.body;
            this.candidates[0].current_round =this.MapShowValues[this.candidates[0].current_round];
            // console.log('CR: ',this.candidates[0].current_round);            
            // console.log(this.candidates);
            this.isLoading = false;
            var flag = false;
            var firstVal = true;
            this.avaliableStatus = [];
            this.InterviewDetails.Current_Status = this.candidates[0].current_round;
            for (let i in this.statuses) {
              if (this.candidates[0].current_round == this.statuses[i]) {
                flag = true;
              } 
              else if (flag) {
                this.avaliableStatus.push(this.statuses[i]);
                if (firstVal) {
                  this.InterviewDetails.Next_Status = this.statuses[i];
                  firstVal = false;
                }
              } else {
              }
            }
            if(!this.isAdmin){
              this.removeAvailableStatus('RELEASE OFFER');
              this.removeAvailableStatus('BOSS ROUND')
            }
          } else {
            this.isLoading = false;
            this.isSearchable = true;
            // this.errorMessage = 'Empty response no candidate retrieved !';
            // this.toggleAlertModal.nativeElement.click();
            alert('Empty response no candidate retrieved !');
          }
          //console.log(this.candidates);
        },
        (err) => {
          // console.log(err);
          this.isLoading = false;
          this.isSearchable = true;
          // this.errorMessage = 'Unable to fetch candidate !';
          // this.toggleAlertModal.nativeElement.click();
          alert('Unable to fetch candidate !');
        }
      );
    } else {
      this.isLoading = false;
      this.isSearchable = true;
      // this.errorMessage = 'Invalid Search Input !';
      // this.toggleAlertModal.nativeElement.click();
      alert('Invalid Search Input !');
    }
  }
  fieldCapture(variable, fieldName, event) {
    //console.log(fieldName);
    this[variable][fieldName] = event.target.value.trim();
    //console.log(this[variable]);
  }
  interviewResult(result) {
    this.isLoading = true;
    this.selectCandidateModal.nativeElement.click();
    this.rejectCandidateModal.nativeElement.click();
    if (this.InterviewDetails.Name.trim().length) {
      var reqBody = {
        registration_no: this.candidates[0].registration_no,
        interviewer: this.InterviewDetails.Name,
        feedback: this.InterviewDetails.FeedBack,
        result: String(result),
        current_round: this.MapActualValues[this.candidates[0].current_round],
        next_round: (result == 'REJECT') ? "REJECTED" : this.MapActualValues[this.InterviewDetails.Next_Status],
        rating: (this.MapActualValues[this.candidates[0].current_round] == 'MANUAL_PRI') ? this.InterviewDetails.Rating : this.candidates[0].rating ,
        username: localStorage.getItem('USER'),
      };
      // console.log(reqBody);
      this.api.submitInterviewResult(reqBody).subscribe(
        (res: any) => {
          this.isLoading = false;
          this.isSearchable = true;
          alert(res.body.message);
          this.candidates= [];
          this. InterviewDetails = {
            Name: '',
            Current_Status: '',
            Next_Status: '',
            FeedBack: '',
            Rating: ''
          };
          //window.location.reload();
          //console.log(this.statuses);
        },
        (err) => {
          this.isLoading = false;
          // console.log(err);
          // this.errorMessage = 'Unable to submit interview result !';
          // this.toggleAlertModal.nativeElement.click();
          alert('Unable to submit interview result !');
        }
      );
    } else {
      this.isLoading = false;
      // this.errorMessage = 'Please enter interviewer name !';
      // this.toggleAlertModal.nativeElement.click();
      alert('Please enter interviewer name !');
    }
  }
  releaseCandidate() {
    this.isLoading = true;
    this.skipCandidateModal.nativeElement.click();
    this.api
      .releaseCandidateRecord(this.candidates[0].registration_no)
      .subscribe(
        (res: any) => {
          this.isLoading = false;
          this.isSearchable = true;
          alert(res.body.message);
          this.candidates= [];
          this. InterviewDetails = {
            Name: '',
            Current_Status: '',
            Next_Status: '',
            FeedBack: '',
            Rating: ''
          };
          //window.location.reload();
          //console.log(this.statuses);
        },
        (err) => {
          this.isLoading = false;
          // console.log(err);
          // this.errorMessage = 'Unable to skip profile !';
          // this.toggleAlertModal.nativeElement.click();
          alert('Unable to skip profile !');
        }
      );
  }

  removeAvailableStatus(value){
    const index: number = this.avaliableStatus.indexOf(value);
    this.avaliableStatus.splice(index, 1);
  }

  removeStatus(value) {
    const index: number = this.statuses.indexOf(value);
    this.statuses.splice(index, 1);
  }
}
export class Candidate {
  registration_no: string;
	full_name: string;
	email_id: string;
	mobile_no: string;
	primary_skills: string;
	experience: string;
	qualification: string;
	year_of_completion: string;
	resume_location: string;
  current_round: string;
  rating: string;
}

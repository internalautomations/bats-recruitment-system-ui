import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpResponse,
} from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Token } from './components/struct/token';
import { Candidate } from './components/struct/candidate';
// import { User } from './components/struct/user';
// import { Project } from './components/struct/project';
// import { Subscription } from './components/struct/subscription';
// import { Research } from './components/struct/research';
// import { UserForProject } from './components/struct/userForProject';
// import { Organizations } from './components/struct/organizations';

@Injectable({
  providedIn: 'root',
})
export class ConfigService {
  httpUrl = {
    dev: 'http://recruitment-system-internal-dev.us-e2.cloudhub.io',
    prod: 'http://recruitment-system-internal.us-e2.cloudhub.io',
  };
  active = this.httpUrl.dev;

  constructor(private http: HttpClient) {}

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  auth(email: string, pass: string): Observable<HttpResponse<Token>> {
    return this.http.get<Token>(this.active + '/api/auth', {
      observe: 'response',
      params: { username: email, password: pass },
    });
  }

  getAllStatus(): Observable<HttpResponse<[]>> {
    return this.http.get<[]>(this.active + '/api/roundlist', {
      observe: 'response',
    });
  }

  getCandidate(filter): Observable<HttpResponse<Candidate[]>> {
    return this.http.get<Candidate[]>(this.active + '/api/fetchrecord', {
      observe: 'response',
      params: filter,
    });
  }

  submitInterviewResult(reqBody) {
    return this.http.post<Candidate[]>(
      this.active + '/api/interview',
      reqBody,
      {
        observe: 'response',
      }
    );
  }

  releaseCandidateRecord(candRegNo) {
    return this.http.post<Candidate[]>(
      this.active + '/api/releaselock',
      {},
      {
        observe: 'response',
        params: { reg_no: candRegNo },
      }
    );
  }

  // getAllUsers(): Observable<HttpResponse<User[]>>{
  //   return this.http.get<User[]>('http://adm-onboarding-mule-app.us-w1.cloudhub.io/api/resources', {
  //     observe: 'response',
  //     params: {requestBy: "angular"}
  //   })
  // }

  // addUser(newUser: User): Observable<User> {
  //   return this.http.post<User>('http://adm-onboarding-mule-app.us-w1.cloudhub.io/api/resources', newUser, {headers: {'Content-Type': 'application/json'}, params: {requestBy: "angular"}})
  // }

  // editUser(user: Object): Observable<Object> {
  //   return this.http.put<Object>('http://adm-onboarding-mule-app.us-w1.cloudhub.io/api/resources', user, {headers: {'Content-Type': 'application/json'}, params: {requestBy: "angular"}})
  // }

  // getAllProjects(): Observable<HttpResponse<Project[]>>{
  //   return this.http.get<Project[]>('http://adm-onboarding-mule-app.us-w1.cloudhub.io/api/projects', {
  //     observe: 'response',
  //     params: {requestBy: "angular"}
  //   })
  // }

  // addProject (newProject :Object): Observable<Object>{
  //   return this.http.post<Object>('http://adm-onboarding-mule-app.us-w1.cloudhub.io/api/projects', newProject,{headers: {'Content-Type': 'application/json'}, params: {requestBy: "angular"}})
  // }

  // assignProject (assignNewProject :Object): Observable<Object>{
  //   return this.http.post<Object>('http://adm-onboarding-mule-app.us-w1.cloudhub.io/api/subscriptions', assignNewProject,{headers: {'Content-Type': 'application/json'}, params: {requestBy: "angular"}})
  // }

  // changeProjectPreferences (newProjectPreferences :Object): Observable<Object>{
  //   return this.http.post<Object>('http://adm-onboarding-mule-app.us-w1.cloudhub.io/api/subscriptions', newProjectPreferences,{headers: {'Content-Type': 'application/json'}, params: {requestBy: "angular"}})
  // }

  // getAllSubscriptions(id: Number): Observable<HttpResponse<Subscription[]>>{
  //   return this.http.get<Subscription[]>('http://adm-onboarding-mule-app.us-w1.cloudhub.io/api/subscriptions', {
  //     observe: 'response',
  //     params:{
  //       requestBy: "angular",
  //       uid: String(id)
  //     }
  //   })
  // }

  // getAllUsersForProject(id: Number): Observable<HttpResponse<UserForProject[]>>{
  //   return this.http.get<UserForProject[]>('http://adm-onboarding-mule-app.us-w1.cloudhub.io/api/usersForProject', {
  //     observe: 'response',
  //     params:{
  //       requestBy: "angular",
  //       jid: String(id)
  //     }
  //   })
  // }

  // generateReportForProject(jid: Number, sendToClient: String):Observable<HttpResponse<[]>>{
  //   return this.http.get<[]>('http://adm-weekly-report-generation.us-w1.cloudhub.io/reports', {
  //     observe: 'response',
  //     params:{
  //       ProjectId: String(jid),
  //       SendToClient: String(sendToClient)
  //     }
  //   })
  // }

  // getProjectReportStatus(id: Number): Observable<HttpResponse<UserForProject[]>>{
  //   return this.http.get<UserForProject[]>('http://adm-onboarding-mule-app.us-w1.cloudhub.io/api/projectReportStatus', {
  //     observe: 'response',
  //     params:{
  //       requestBy: "angular",
  //       jid: String(id)
  //     }
  //   })
  // }

  // toggleProjectReportsStatus(jid: Number, category: String):Observable<Object>{
  //   return this.http.put('http://adm-onboarding-mule-app.us-w1.cloudhub.io/api/toggleReportStatus',"",
  //    { params:{
  //       requestBy: "angular",
  //       jid: String(jid),
  //       category: String(category)
  //     }}
  //   )
  // }

  // fetchReportDataFromAWS():Observable<HttpResponse<[]>>{
  //   return this.http.get<[]>('http://adm-weekly-report-generation.us-w1.cloudhub.io/reportdata', {
  //     observe: 'response',
  //   })
  // }

  // fetchHistoricalReportData(date: String):Observable<HttpResponse<[]>>{
  //   return this.http.get<[]>('http://adm-weekly-report-generation.us-w1.cloudhub.io/reportdata', {
  //     observe: 'response',
  //     params:{
  //       searchdate: String(date)
  //     }
  //   })
  // }

  // reportDownload(jid: Number):Observable<any>{
  //   return this.http.get('http://adm-weekly-report-generation.us-w1.cloudhub.io/projectreport', {
  //     responseType: 'blob',
  //  // observe: 'response',
  //     params:{
  //       id : String(jid)
  //     }
  //   });
  // }
}
